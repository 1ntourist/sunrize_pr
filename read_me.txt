1. git clone with gitlab-link
2. cd sunrise_pr
3. python3 -m venv env
4. source env/bin/activate
5. pip install -r requirements.txt
6. With your Postgres role, make sunrize_db, and edit DB-name, USER, PASSWORD in setting.py (in folder 'sunrise_project'
7. in direction with manage.py:
    ./manage.py migrate
    ./manage.py makemigrations
    ./manage.py createsuperuser
    ./manage.py runserver
8. add some products to DB with your superuser
    use http://127.0.0.1:8000/admin/