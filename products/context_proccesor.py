from .models import *


def get_product_category(request):
    return {'product_category_context': Category.objects.all()}


def get_product(request):
    return {'product_context': Product.objects.all()}

